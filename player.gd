extends KinematicBody2D

var blue_bullet = preload("res://blue_bullet.tscn")
var red_bullet = preload("res://red_bullet.tscn")
var green_bullet = preload("res://green_bullet.tscn")
var pink_bullet = preload("res://pink_bullet.tscn")

var current_bullet = 0
var bullets = ['pink']

var walk_speed = 100
var walk_max_speed = 500
var speed = 0

var jump_speed = 700
var jump_height = 0
var jump_max_height = 150
var jump_duration = 0.5
var air_decceleration = 2000;

var gravity_speed = 200

var height = 0

enum STATES {
	IDLE,
	WALK,
	JUMP,
	PRE_JUMP,
	PRE_GRAB_JUMP,
	FALL,
	GRAB
}

var state = IDLE

func _ready():
	$bullet_label.text = bullets[current_bullet]
	pass

func _process(delta):
	var input = Vector2()
	input.x = -1 if Input.is_action_pressed('move_left') else 0 + 1 if Input.is_action_pressed('move_right') else 0
	input = input.normalized()
	
	var velocity = Vector2()
	
	speed = min(speed + walk_speed, walk_max_speed)
	
	if state in [WALK, JUMP, FALL]:
		if Input.is_action_pressed('move_left') && $pivot.scale.x > 0:
			$pivot.scale.x = -$pivot.scale.x;
		if Input.is_action_pressed('move_right') && $pivot.scale.x < 0:
			$pivot.scale.x = -$pivot.scale.x;
	
	match state:
		IDLE:
			if not $floor.is_colliding():
				_change_state(FALL)
			elif Input.is_action_pressed('jump'):
				_change_state(PRE_JUMP)
			elif input:
				_change_state(WALK)
				
		WALK:
			if not $floor.is_colliding():
				_change_state(FALL)
			elif Input.is_action_pressed('jump'):
				_change_state(PRE_JUMP)
			elif not input:
				_change_state(IDLE)
				
		JUMP:
			#print(jump_height * delta)
			#print(air_decceleration * delta)
			#velocity.y -= air_decceleration * delta
			velocity.y -= jump_height
			jump_height -= air_decceleration * delta
			if jump_height < 0:
				_change_state(FALL)
			#if $floor.is_colliding():
			#	_change_state(FALL)
			#velocity.y -= jump_speed
			#jump_height += jump_speed * delta
			#if (jump_height > jump_max_height):
			#	_change_state(FALL)
		FALL:
			#print(jump_height)
			if $floor.is_colliding():
				_change_state(IDLE)
			elif $left_wall.is_colliding() || $right_wall.is_colliding():
				_change_state(GRAB)
			else:
				#velocity.y += jump_speed
				velocity.y += jump_height
				jump_height += gravity_speed * delta
		GRAB:
			if Input.is_action_pressed('jump'):
				_change_state(PRE_GRAB_JUMP)
			elif input:
				if (Input.is_action_pressed('move_left') && $right_wall.is_colliding()) || (Input.is_action_pressed('move_right') && $left_wall.is_colliding()):
					_change_state(FALL)
	
	velocity += input * speed
	move_and_slide(velocity)

func _input(event):
	if event is InputEventMouseButton:
		if event.pressed:
			if event.button_index == BUTTON_LEFT:
				var bullet = null
				match bullets[current_bullet]:
					'pink':
						bullet = pink_bullet.instance()
					'red':
						bullet = red_bullet.instance()
					'blue':
						bullet = blue_bullet.instance()
					'green':
						bullet = green_bullet.instance()
				bullet.position = $spawn.global_position
				bullet.direction = (get_global_mouse_position() - $spawn.global_position).normalized()
				bullet.z_index = 10
				get_parent().add_child(bullet)
			if event.button_index == BUTTON_WHEEL_UP:
				current_bullet += 1
				if current_bullet >= bullets.size():
					current_bullet = bullets.size() - 1
				print(bullets)
				$bullet_label.text = bullets[current_bullet]
				print(current_bullet)
			if event.button_index == BUTTON_WHEEL_DOWN:
				current_bullet -= 1
				if current_bullet < 0:
					current_bullet = 0
				$bullet_label.text = bullets[current_bullet]
				print(current_bullet)

func _change_state(new_state):
	match state:
		JUMP:
			jump_height = 0
		FALL:
			jump_height = 0
		GRAB:
			$pivot/AnimatedSprite.offset.x = 0
	
	match new_state:
		IDLE:
			$state_label.text = 'IDLE'
			$pivot/AnimatedSprite.play('idle')
		WALK:
			$state_label.text = 'WALK'
			$pivot/AnimatedSprite.play('marcher')
		PRE_JUMP:
			$state_label.text = 'PRE_JUMP'
			$pivot/AnimatedSprite.play('sauter')
			$jump_timer.start()
		PRE_GRAB_JUMP:
			$state_label.text = 'PRE_GRAB_JUMP'
			_set_an_sprt_grab()
			$pivot/AnimatedSprite.play('paroie_sauter')
			$jump_timer.start()
		JUMP:
			$state_label.text = 'JUMP'
			jump_height += jump_speed
			#$jump_tween.interpolate_method(self, '_jump_teeew_method', 0, 1, jump_duration, Tween.TRANS_LINEAR, Tween.EASE_IN)
			#$jump_tween.start()
		FALL:
			$state_label.text = 'FALL'
			$pivot/AnimatedSprite.play('tomber')
		GRAB:
			$state_label.text = 'GRAB'
			_set_an_sprt_grab()
			
			if $right_wall.is_colliding() && $pivot.scale.x > 0:
				$pivot.scale.x = -$pivot.scale.x;
			if $left_wall.is_colliding() && $pivot.scale.x < 0:
				$pivot.scale.x = -$pivot.scale.x;
			
			$pivot/AnimatedSprite.play('paroie')
	
	state = new_state

func _set_an_sprt_grab():
	if $right_wall.is_colliding():
		$pivot/AnimatedSprite.offset.x = 107
	if $left_wall.is_colliding():
		$pivot/AnimatedSprite.offset.x = 87.5

var last_value = 0

func _on_AnimatedSprite_animation_finished():
	pass

func _on_jump_timer_timeout():
	_change_state(JUMP)

func _jump_teeew_method(progress):
	var value = (pow(sin(progress * PI), 0.5) * 1)# - last_value
	if last_value > value:
		pass # change to fall !
	print('jump_speed : ',jump_height,' diff: ', (last_value-value))
	jump_height += (last_value-value)
	last_value = value
	

func _on_jump_tween_tween_completed(object, key):
	print('fini !!!!')
	_change_state(FALL)
