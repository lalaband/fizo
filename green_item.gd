extends Area2D

export(String, 'green', 'pink', 'blue', 'red') var item_name

var played = false

func _ready():
	pass


func _on_item_body_entered(body):
	if body.name == 'player' and not played:
		$AnimationPlayer.play('disparition')
		played = true
		body.bullets.append(item_name)


func _on_AnimationPlayer_animation_finished(anim_name):
	queue_free()
