extends Area2D

var direction = Vector2()
var speed = 200
var has_collided = false

func _ready():
	pass

func _process(delta):
	if not has_collided:
		position += direction * speed * delta


func _on_blue_bullet_body_entered(body):
	if body.name != 'player' and not has_collided:
		$AnimationPlayer.play("grossir")
		has_collided = true


func _on_AnimationPlayer_animation_finished(anim_name):
	queue_free()

